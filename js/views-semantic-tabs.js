/**
 * @file
 * Applies tabs on views semantic tabs display.
 */

(function ($, Drupal) {
  Drupal.behaviors.views_semantic_tabs = {
    attach: function (context, settings) {
      $.each(settings.views_semantic_tabs, function (key, config) {
        $(once('views_semantic_tabs--' + key, "#" + key)).tabs(config);
      });
    }
  };
})(jQuery, Drupal);
