# Views semantic tabs

This module provides a views style plugin to display views results in jQuery
UI Tabs.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_semantic_tabs).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_semantic_tabs).


## Requirements
Previous versions of the module did not require any modules outside of Drupal core.

### Version 3.x
Relies on the jQuery UI script, [which is no longer part of Drupal Core as of version 8.8.0](https://www.drupal.org/node/3067969), requiring the installation of modules `jquery_ui` and `jquery_ui_tabs`.


## Installation

Install as you would normally install a contributed Drupal module. See
[Installing Drupal Modules](https://drupal.org/documentation/install/modules-themes/modules-8)
for further information.


## Configuration

Update your view with semantic tabs style under format style in the view.


## Maintainers

- [Marcos Hollanda (mabho)](https://www.drupal.org/u/mabho)
- [Krishna kanth (krknth)](https://drupal.org/u/krknth)
- [Mahaveer Singh Panwar (mahaveer003)](https://www.drupal.org/u/mahaveer003)
- [Neeraj kumar (neerajskydiver)](https://drupal.org/u/neerajskydiver)
- [Nishant kumar (nishantkumar155)](https://www.drupal.org/u/nishantkumar155)
- [CLAUSE Dominique (Dom.)](https://drupal.org/u/dom)
